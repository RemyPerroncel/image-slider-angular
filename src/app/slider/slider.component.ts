import { timer } from 'rxjs';
import { Image } from '../models/image.model';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { TransitionEffectEnum } from '../enums/transition-effect.enum';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit, OnDestroy {

  @Input()
  public imagesSrc: Image[] = [];

  @Input()
  public transitionMode: TransitionEffectEnum = TransitionEffectEnum.SLIDE; // Slide effect by default

  public onOff = true;
  public currentIndex = 0; // Index of imagesSrc array

  private readonly SLIDE_DURATION = 3;
  private timer;
  private subscription;

  constructor() {
  }

  ngOnInit() {
    this.timer = timer(0, 1000);
    this.startSlider();
  }

  public getElementClass(element: Image): string {
    if (this.currentIndex === this.imagesSrc.indexOf(element)) {
      return `slider__container__element active ${this.transitionMode}`;
    }
    return `slider__container__element ${this.transitionMode}`;
  }

  public change(idEl: number): void {
    this.currentIndex = idEl;
    if (this.onOff) {
      this.resetTimer();
    }
  }

  public onOffSlider(): void {
    this.onOff = !this.onOff;
    if (this.onOff) {
      this.startSlider();
    }
  }

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  private startSlider(): void {
    this.subscription = this.timer.subscribe(t => {
      if (t === this.SLIDE_DURATION && this.onOff) {
        if (this.currentIndex === this.imagesSrc.length - 1) {
          this.currentIndex = 0;
        } else {
          this.currentIndex++;
        }
        this.resetTimer();
      } else if (!this.onOff) {
        this.ngOnDestroy();
      }
    });
  }

  private resetTimer(): void {
    this.ngOnDestroy();
    this.startSlider();
  }
}
