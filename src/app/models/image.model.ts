export interface ImageDTO {
  id: string;
  path: string;
  libelle?: string;
}

export class Image {
  id: number;
  path: string;
  libelle?: string;

  constructor(dto: Partial<ImageDTO>) {
    Object.assign(this, dto);
  }
}
