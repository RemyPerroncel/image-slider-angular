import { Observable, of } from 'rxjs';
import { Image, ImageDTO } from '../models/image.model';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class HomeService {

  public getImages(): Observable<Image[]> {
    const dataImages: ImageDTO[] = [
      { id: '1', path: '../../assets/jpg/image-1.jpg', libelle: 'Image n°1' },
      { id: '2', path: '../../assets/jpg/image-2.jpg', libelle: 'Image n°2' },
      { id: '3', path: '../../assets/jpg/image-3.jpg', libelle: 'Image n°3' },
      { id: '4', path: '../../assets/jpg/image-4.jpg', libelle: 'Image n°4' },
      { id: '5', path: '../../assets/jpg/image-5.jpg', libelle: 'Image n°5' },
      { id: '6', path: '../../assets/jpg/image-6.jpg', libelle: 'Image n°6' }
    ];
    const images: Image[] = [];
    dataImages.forEach(img => {
      images.push(new Image(img));
    });
    return of(images);
  }
}
