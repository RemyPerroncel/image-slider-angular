import { HomeService } from './home.service';
import { Component, OnInit } from '@angular/core';
import { Image } from '../models/image.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public imageData: Image[] = [];

  constructor(private homeService: HomeService) {
  }

  public ngOnInit(): void {
    this.homeService.getImages().subscribe(data => this.imageData = data);
  }
}
