export enum TransitionEffectEnum {
  FADE = 'FADE',
  SLIDE = 'SLIDE',
}
